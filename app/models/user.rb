require 'bcrypt'
class User < ActiveRecord::Base
  include BCrypt

  EMAIL_REGEX = /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  MAX_COLUMN_LENGTH = 255
  attr_accessor :password, :password_confirmation

  
  validates :password, presence: true, if: :password_required?
  validates :password, length: { minimum: 6, maximum: 20 }, allow_blank: true

  validates :email,
            format: { with: EMAIL_REGEX }
  validates :email, presence: true
  validates :email,
            uniqueness: { case_sensitive: false},
            allow_blank: true
  validates :email, length: { maximum: MAX_COLUMN_LENGTH }

  validates :password, confirmation: true, if: :password_required?
  validates :password_confirmation, presence: true, if: :password_required?

  before_save :encrypt_password

  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def password_required?
    !persisted? || !password.nil?
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
end