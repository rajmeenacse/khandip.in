require 'spec_helper.rb'

describe User do
  context 'with all valid attributes' do
	  before(:each) do
	    @attr = { email: 'example@test.com',
	              password: 'secret', 
	              password_confirmation: 'secret' }
	  end

		describe "password encryption" do
	    let(:user) { User.create!(@attr) }

	    it "should have an password hash" do
	      user.should respond_to(:password_hash)
	    end

	    it "should have an password salt" do
	      user.should respond_to(:password_salt)
	    end
		end
  end		
end
