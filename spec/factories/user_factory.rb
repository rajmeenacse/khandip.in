require 'factory_girl'

FactoryGirl.define do
  factory :user do
    email 'example@test.com'
    password  'secret'
    password_confirmation { |u| u.password }
  end
end